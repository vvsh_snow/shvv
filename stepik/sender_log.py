import os
from datetime import datetime, timedelta
#----------------------------------------------------------------------
def Init(logfilenameprefix):
  global logfilename
  logfilename = datetime.now().strftime('%y%m%d-%H%M')
  logfilename = '%s-$%x-%s.log' % (logfilenameprefix, os.getpid(), logfilename)

  logpath = os.path.dirname(os.path.abspath(__file__))
  logpath = os.path.join(logpath, "tmp")
  logfilename = os.path.join(logpath, logfilename)

  if not os.path.exists(logpath):
    os.makedirs(logpath)

  for file in os.listdir(logpath):
    f = os.path.join(logpath, file)
    if os.path.isfile(f) and file.endswith('.log'):
      t = datetime.fromtimestamp(os.path.getmtime(f))
      t_old = datetime.now() - timedelta(days=30)
      if t < t_old:
        try:
          print ('Try remove: ' + file)
          os.remove(f)
        except Exception as E:
          print(E)

#----------------------------------------------------------------------
def Insert(logStr):
  with open(logfilename, 'a', encoding='utf-8') as f_obj:
    string = datetime.now().strftime('%y%m%d-%H%M%S-%f')[:-3] + ': ' + logStr
    print(string)
    f_obj.write(string + '\n')
    f_obj.close()
#----------------------------------------------------------------------
