import requests
import json

with open('c:\PY\stepik-in19.txt') as inf, open('c:\PY\stepik-out19.txt', 'w', encoding = 'utf-8') as outf:
    client_id = 'b4e4491dc6b356a46fba'
    client_secret = '3e1f43ab067e0e25c6465368aed93acd'    
    r = requests.post("https://api.artsy.net/api/tokens/xapp_token",
                      data={
                          "client_id": client_id,
                          "client_secret": client_secret
                      })

    x = json.loads(r.text)
    token = x["token"]
    headers = {"X-Xapp-Token" : token}

    m = {}
    for i in inf:
        s = "https://api.artsy.net/api/artists/{}".format(i.strip())
        print(s)

        r = requests.get(s, headers=headers)
        r.encoding = 'utf-8'
        if not r: continue
        if r.status_code != 200: continue
        x = json.loads(r.text)
        m[x['sortable_name']]=x['birthday']

    for i in sorted(m.items(), key=lambda x: (x[1], x[0])):
        print(i[0], file=outf)
    print('OK')


