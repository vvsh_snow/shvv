import smtplib
 
def send_email(host, subject, to_addr, from_addr, body_text):
    """
    Send an email
    """
 
    BODY = "\r\n".join((
        "From: %s" % from_addr,
        "To: %s" % to_addr,
        "Subject: %s" % subject ,
        "",
        body_text
    ))
 
    server = smtplib.SMTP(host)
    server.login("v1001@test.tst", "11111")
    server.sendmail(from_addr, [to_addr], BODY)
    server.quit()
 
 
if __name__ == "__main__":
    host = "192.168.181.154"
    subject = "Test email from Python"
    to_addr = "v1000@test.tst"
    from_addr = "v1001@test.tst"
    body_text = "Python rules them all!"
    send_email(host, subject, to_addr, from_addr, body_text)
