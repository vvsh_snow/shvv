import sender_settings as cfg
import sender_email as mail
import sender_log as log
import cx_Oracle as db
import time
#----------------------------------------------------------------------
def getWaitInterval():
  fmt = '%H:%M'
  curtime = time.strptime(time.strftime(fmt, time.localtime()), fmt)
  ntimefrom = time.strptime(cfg.night_time_from, fmt)
  ntimeto = time.strptime(cfg.night_time_to, fmt)
  dtimefrom = time.strptime(cfg.day_time_from, fmt)
  dtimeto = time.strptime(cfg.day_time_to, fmt)

  if curtime >= dtimefrom and curtime <= dtimeto:
    return int(cfg.day_timer_interval)
  elif not (curtime >= ntimeto and curtime <= ntimefrom):
    return int(cfg.night_timer_interval)
  else:
    return 5
#----------------------------------------------------------------------
def main():
  while True:
    try:
      while True:
        log.Insert ('Connect to DB')
        conn = db.connect(cfg.dblogin, cfg.dbpwd, cfg.dbdsn)

        cur = conn.cursor()

        cur.execute("select sys_context('USERENV', 'TERMINAL'),"
                  + "sys_context('USERENV', 'IP_ADDRESS'),"
                  + "sys_context('USERENV', 'OS_USER'),"
                  + "sys_context('USERENV', 'SESSIONID') from dual");

        res = cur.fetchall()

        for row in res:
          lock_value = "LOCK:{}-{}-{}-{}".format(row[0],row[1],row[2],row[3]);

        log.Insert ('Find messages')

        cur.execute("UPDATE post_messages "
                  + "SET msg_sent=NULL, msg_sentresult='%s' " % lock_value
                  + "WHERE rownum=1 "
                  + "AND (msg_sentresult = ' ' OR msg_sentresult = 'ERROR') AND msg_to LIKE '%test%'")
        conn.commit()

        if not cur.rowcount:
          conn.close()
          time.sleep(getWaitInterval())
          continue

        cur.execute("SELECT rowid, msg_to, msg_from, msg_subject, msg_text, msg_attach_name, msg_attach, msg_id "
                  + "FROM post_messages t "
                  + "WHERE msg_sentresult='%s' AND rownum=1" % lock_value)

        res = cur.fetchall()

        for row in res:

          try:
            body_text = row[4].read() if row[4] else ''
            body_file = bytes(row[6].read(), 'cp1251') if row[6] and row[5] else ''
            file_to_attach = row[5] if body_file else ''

            log.Insert ('Try send mail')
            mail.send_email_with_attachment(
              row[3],
              body_text,
              row[1],
              row[2],
              file_to_attach,
              cfg.smtphost,
              cfg.smtplogin,
              cfg.smtppwd,
              body_file)
            sent_result = 'OK';
          except Exception as E:
            log.Insert ('ERROR: ' + str(E))
            sent_result = 'ERROR'
            time.sleep(2)
          finally:
            log.Insert ('%s: %s -> %s: %s' % (row[7], row[1], row[2], sent_result))

            cur.execute("UPDATE post_messages "
                      + "SET msg_sent=sysdate, msg_sentresult='%s' " % sent_result
                      + "WHERE rowid='%s'" % row[0])
            conn.commit()
        conn.close()
    except Exception as E:
      log.Insert ('ERROR: ' + str(E))
      time.sleep(5)
#----------------------------------------------------------------------
if __name__ == "__main__":
  log.Init ('sender')
  log.Insert ('Init')
  cfg.init_settings()
  log.Insert ('settingsDB: dsn:%s login:%s' % (cfg.dbdsn, cfg.dblogin))
  log.Insert ('settingsMail: host:%s login:%s' % (cfg.smtphost, cfg.smtplogin))
  log.Insert ('Start')
  main();
#----------------------------------------------------------------------
