class multifilter:
    def judge_half(pos, neg):
        return pos >= neg 

    def judge_any(pos, neg):
        return pos >= 1

    def judge_all(pos, neg):
        return neg == 0 and pos > 0

    def __init__(self, iterable, *funcs, judge=judge_any):
        self.x = iterable
        self.f = funcs
        self.j = judge
        self.i = 0

    def __iter__(self):
        self.i = 0
        return self

    def __next__(self):
        while self.i < len(self.x):
            x, p, n = self.x[self.i], 0, 0
            for f in self.f:
                if f(x): p += 1
                else: n += 1
            self.i += 1
            if self.j(p, n): return x
            continue
        raise StopIteration
        
def mul2(x):
    return x % 2 == 0

def mul3(x):
    return x % 3 == 0

def mul5(x):
    return x % 5 == 0


a = [i for i in range(31)] # [0, 1, 2, ... , 30]

print(list(multifilter(a, mul2, mul3, mul5))) 
# [0, 2, 3, 4, 5, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30]

print(list(multifilter(a, mul2, mul3, mul5, judge=multifilter.judge_half))) 
# [0, 6, 10, 12, 15, 18, 20, 24, 30]

print(list(multifilter(a, mul2, mul3, mul5, judge=multifilter.judge_all))) 
# [0, 30]
