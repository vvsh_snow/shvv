def addklass(m,k,n):
    k=int(k)
    n=int(n)
    if k in m: m[k]+=[n]
    else: m[k]=[n]


inf=open('c:\PY\stepik-in7.txt')
outf=open('c:\PY\stepik-out7.txt', 'w')

l=[]
m={}
for s in inf:
    l=s.strip().split('\t')
    addklass(m,l[0],l[2])

for i in range(1,max(m.keys())+1):
    if i in m:
        v=m[i]
        print(i,sum(v)/len(v),file=outf)
    else: print(i,'-',file=outf)

inf.close()
outf.close()
