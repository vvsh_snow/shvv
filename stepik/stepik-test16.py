import re

def getUrls(pStr):
    res = []
    for m1 in re.finditer(r'<a\s+[^<>]*>', pStr):
        s = re.sub(r'<a\s+', '', m1[0])
        s = re.sub(r'>', '', s)
        m2 = re.search(r'href=[\'"][^\'"]*[\'"]', s)
        if m2:
            s = re.sub(r'href=[\'"]', '', m2[0])
            s = re.sub(r'[\'"]', '', s)
            s = s.strip()
            if res.count(s) > 0: continue
            res.append(s)
    return res

def getHosts(pList):
    res = []
    for i in pList:
        m = re.search(r'\.\./', i)
        if m:
            if m.start() == 0: continue

        m = re.search(r'(ftp|http)://[^/:]+', i)
        if m:    
            s = re.sub(r'(ftp|http)://', '', m[0])
            if res.count(s) > 0: continue
            res.append(s)
    res.sort()
    return res
    
with open('stepik-in16.txt') as inf:
    L = getHosts(getUrls(inf.read()))

for i in L: print(i)

