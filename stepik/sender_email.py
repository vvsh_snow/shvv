import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate
from email.header import Header
#----------------------------------------------------------------------
def send_email_with_attachment(subject,
                               body_text,
                               to_emails,
                               from_email,
                               file_to_attach,
                               smtp_host,
                               smtp_login,
                               smtp_pwd,
                               file_body):

    msg = MIMEMultipart()
    msg["From"] = Header(from_email, 'utf-8')
    msg["To"] = Header(to_emails, 'utf-8')
    msg["Subject"] = subject
    msg["Date"] = formatdate(localtime=True)

    if body_text:
        msg.attach( MIMEText(body_text, 'plain', 'utf-8') )

    if file_body and file_to_attach:
        attachment = MIMEBase('application', 'octet-stream; charset="cp1251"')

        attachment.set_payload(file_body)
        encoders.encode_base64(attachment)
        attachment.add_header('Content-Disposition', 'attachment', filename=file_to_attach)
        msg.attach(attachment)

    #emails = 'v1001@test.tst' # T E S T
    emails = to_emails
    server = smtplib.SMTP(smtp_host)
    server.login(smtp_login, smtp_pwd)
    server.sendmail(smtp_login, emails, msg.as_string())
    server.quit()
#----------------------------------------------------------------------
