import requests
import json

with open('c:\PY\stepik-in18.txt') as inf, open('c:\PY\stepik-out18.txt', 'w') as outf:
    for i in inf:
        s = "http://numbersapi.com/{}/math?json=true".format(i.strip())
        print(s)
        r = requests.get(s)
        if not r: continue
        if r.status_code != 200: continue
        x = json.loads(r.text)
        if x['found']: y = 'Interesting'
        else: y = 'Boring'
        print(y, file=outf)
    print('OK')

