import itertools

def primes():
    a = 1
    while True:
        a += 1
        for i in range(2, a-1):
            if a % i == 0: break
        else: yield a
        
print(list(itertools.takewhile(lambda x: x <= 31, primes())))
# [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31]
