import os
from configparser import ConfigParser


def init_settings():
    base_path = os.path.dirname(os.path.abspath(__file__))
    config_path = os.path.join(base_path, "sender.ini")

    if not os.path.exists(config_path):
        raise Exception('File "{}" not found!'.format(config_path))

    cfg = ConfigParser()
    cfg.read(config_path)

    global dblogin, dbpwd, dbdsn;
    global smtplogin, smtppwd, smtphost;
    global day_time_from, day_time_to, day_timer_interval
    global night_time_from, night_time_to, night_timer_interval

    dbdsn   = cfg.get("DB", "dsn")
    dblogin = cfg.get("DB", "login")
    dbpwd   = cfg.get("DB", "pwd")

    smtphost   = cfg.get("SMTP", "host")
    smtplogin  = cfg.get("SMTP", "login")
    smtppwd    = cfg.get("SMTP", "pwd")

    day_time_from = cfg.get("SC", "day_time_from", fallback="6:00")
    day_time_to   = cfg.get("SC", "day_time_to", fallback="23:00")
    day_timer_interval = cfg.get("SC", "day_timer_interval", fallback="5")

    night_time_from  = cfg.get("SC", "night_time_from", fallback="23:00")
    night_time_to    = cfg.get("SC", "night_time_to", fallback="6:00")
    night_timer_interval = cfg.get("SC", "night_timer_interval", fallback="600")
#----------------------------------------------------------------------
