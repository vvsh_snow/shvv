from smtplib import SMTPException
from django.db.models import Count, Max
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from django.template import Template, Context, Engine
import mysite.settings as settings
from mysite.celery import app
from .models import Comment


@app.task(bind=True, default_retry_delay=10 * 60)
def send_new_comment_notify(self, author):
    user_model = get_user_model().objects.get(pk=author)
    recipient = user_model.email
    recip_name = user_model.username
    comments = Comment.objects.filter(post__author=author,
                                     is_sent=False).values_list('id',
                                                                flat=True)
    msg_count = len(comments)
    posts = Comment.objects.filter(post__author=author, is_sent=False).values(
        'post__id', 'post__title').annotate(cnt=Count('id'))
    subject = 'You have %d new comments' % msg_count

    message = Engine.get_default().get_template(
        'blog/comment_notify.txt').render(
        context=Context({'name': recip_name, 'posts': posts}))
    # print(posts.query)  # SQL-debug
    try:
        send_mail(
            subject=subject,
            message=message,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[recipient],
            fail_silently=False,
            html_message=[])
    except SMTPException as ex:
        self.retry(exc=ex)
    else:
        Comment.objects.filter(pk__in=comments).update(is_sent=True)


@app.task(ignore_result=True)
def send_reports():
    rows = Comment.objects.filter(is_sent=False).values(
        'post__author').annotate(cnt=Count('id'),
                                 last_time=Max('created_date'))
    # print(rows.query)  # SQL-debug
    for row in rows:
        send_new_comment_notify.delay(row['post__author'])
